import argparse
import sys
import os
from ytdl.youtube import Youtube

import logging


def parse_args():
    # Instantiate the parser
    parser = argparse.ArgumentParser(description="Youtube Downloader")
    parser.add_argument(
        "--name",
        type=str,
        help="the name of the channel to lookup",
    )
    parser.add_argument("--api-key", type=str, help="a youtube API key")
    parser.add_argument(
        "--debug",
        action="store_true",
        help="enable verbose mode",
    )
    return parser.parse_args()


class LessThanFilter(logging.Filter):
    def __init__(self, exclusive_maximum, name=""):
        super(LessThanFilter, self).__init__(name)
        self.max_level = exclusive_maximum

    def filter(self, record):
        # non-zero return means we log this message
        return 1 if record.levelno < self.max_level else 0


def init_logger(debug):
    logger = logging.getLogger("ytdl")
    logger.setLevel(logging.DEBUG)

    logging_handler_out = logging.StreamHandler(sys.stdout)
    logging_handler_out.setLevel(logging.DEBUG)
    logging_handler_out.addFilter(LessThanFilter(logging.WARNING))
    logger.addHandler(logging_handler_out)

    logging_handler_err = logging.StreamHandler(sys.stderr)
    logging_handler_err.setLevel(logging.WARNING)
    logger.addHandler(logging_handler_err)

    if debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)


if __name__ == "__main__":
    args = parse_args()
    init_logger(args.debug)

    youtube = Youtube(api_key=args.api_key)
    print("------ channel id ------ => channel name (description)")
    for channel in youtube.search_channel(args.name):
        print("%s => %s (%s)" % (channel["id"]["channelId"], channel["snippet"]["title"], channel["snippet"]["description"]))
