import argparse
import sys
import os
from ytdl.videodownloader import VideoDownloader

import logging


def parse_args():
    # Instantiate the parser
    parser = argparse.ArgumentParser(description="Youtube Downloader")
    parser.add_argument(
        "--channel",
        type=str,
        help="the name of the channel which I should download videos",
    )
    parser.add_argument("--api-key", type=str, help="a youtube API key")
    parser.add_argument(
        "--download-path", type=str, nargs="?", help="directory to save video in",
        default=os.getcwd()
    )
    parser.add_argument(
        "--no-download",
        action="store_true",
        help="do not download video, but mark them as downloaded",
    )
    parser.add_argument(
        "--no-update-db",
        action="store_true",
        help="do not update database after downloading a file",
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        help="enable verbose mode",
    )
    return parser.parse_args()


class LessThanFilter(logging.Filter):
    def __init__(self, exclusive_maximum, name=""):
        super(LessThanFilter, self).__init__(name)
        self.max_level = exclusive_maximum

    def filter(self, record):
        # non-zero return means we log this message
        return 1 if record.levelno < self.max_level else 0


def init_logger(debug):
    logger = logging.getLogger("ytdl")
    logger.setLevel(logging.DEBUG)

    logging_handler_out = logging.StreamHandler(sys.stdout)
    logging_handler_out.setLevel(logging.DEBUG)
    logging_handler_out.addFilter(LessThanFilter(logging.WARNING))
    logger.addHandler(logging_handler_out)

    logging_handler_err = logging.StreamHandler(sys.stderr)
    logging_handler_err.setLevel(logging.WARNING)
    logger.addHandler(logging_handler_err)

    if debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)


if __name__ == "__main__":
    args = parse_args()
    init_logger(args.debug)

    vd = VideoDownloader(
        channel=args.channel,
        api_key=args.api_key,
        state_file="download-state.db",
        download=not args.no_download,
        update_db=not args.no_update_db,
        base_download_path=args.download_path,
    )
    vd.start_download()
#        api_key="AIzaSyCkjc8qDJRQ8Vq2dzlUOVm1c4QjSX6IVOw",
