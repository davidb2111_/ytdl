import googleapiclient.discovery
import googleapiclient.errors
import logging
from pprint import pprint

log = logging.getLogger(__name__)


class Youtube:
    def __init__(self, api_key: str):
        """get video from playlists, that consumes less quota points"""
        self.api_key = api_key
        api_service_name = "youtube"
        api_version = "v3"

        self.youtube = googleapiclient.discovery.build(
            api_service_name, api_version, developerKey=self.api_key
        )

    def list_channel(self):
        request = self.youtube.channels().list(
            part="snippet,contentDetails", id=self.name
        )
        return request.execute()["items"][0]

    def list_playlists(self, channel_id: str):
        request = self.youtube.playlists().list(
            part="snippet,contentDetails",
            channelId=channel_id,
            maxResults=25,
        )
        return request.execute()

    def search_all_videos_of_channel(self, channel_id: str):
        pageToken = ""
        data = []
        while True:
            result = (
                self.youtube.search()
                .list(
                    order="date",
                    part="snippet",
                    maxResults=25,
                    channelId=channel_id,
                    pageToken=pageToken if pageToken != "" else "",
                )
                .execute()
            )
            v = result.get("items", [])
            if v:
                data.extend(v)
            pageToken = result.get("nextPageToken")
            if not pageToken:
                break
        return data

    def list_video_of_playlist(self, playlist_id: str):
        pageToken = ""
        data = []
        while True:
            result = (
                self.youtube.playlistItems()
                .list(
                    part="snippet",
                    maxResults=25,
                    playlistId=playlist_id,
                    pageToken=pageToken if pageToken != "" else "",
                )
                .execute()
            )
            v = result.get("items", [])
            if v:
                data.extend(v)
            pageToken = result.get("nextPageToken")
            if not pageToken:
                break
        return data

    def search_channel(self, query):
        request = self.youtube.search().list(
            part="snippet,id", q=query, type="channel"
        )
        return request.execute()["items"]

    def forge_video_url(self, video_id):
        return "https://www.youtube.com/watch?v=" + video_id
