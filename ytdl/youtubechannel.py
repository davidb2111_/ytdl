import googleapiclient.discovery
import googleapiclient.errors
import logging
from ytdl.youtube import Youtube
log = logging.getLogger(__name__)


class YoutubeChannel(Youtube):
    def __init__(self, name: str, api_key: str):
        """get video from playlists, that consumes less quota points"""
        self.name = name
        super().__init__(api_key)

        self._channel_data = {}
        self.channel = self.list_channel()
        #self.playlists = self._list_playlists()
        log.info(
            "Getting channels from %s (id=%s)",
            self.channel["snippet"]["title"],
            name,
        )
        # for playlist in self.playlists["items"]:
        playlist = self._get_uploads_playlist()
        log.info("Getting playlist %s (id=%s)", playlist["title"], playlist["id"])
        self._channel_data[playlist["id"]] = {}
        self._channel_data[playlist["id"]]["title"] = playlist["snippet"]["localized"][
            "title"
        ]
        try:
            videos = self.list_video_of_playlist(playlist["id"])
        except googleapiclient.errors.HttpError:
            log.warning("no video of playlist %s, going to search()", playlist["id"])
            videos = self.search_all_videos_of_channel()

        log.debug("found %d videos", len(videos))
        self._channel_data[playlist["id"]]["items"] = {}
        for video in videos:
            try:
                if video["kind"] == "youtube#searchResult":
                    video_id = video["id"]["videoId"]
                elif video["kind"] == "youtube#playlistItem":
                    video_id = video["snippet"]["resourceId"]["videoId"]

                log.info("Found video %s", video_id)
                self._channel_data[playlist["id"]]["items"][video_id] = {}
                self._channel_data[playlist["id"]]["items"][video_id]["title"] = video[
                    "snippet"
                ]["title"]
                self._channel_data[playlist["id"]]["items"][video_id][
                    "published_at"
                ] = video["snippet"]["publishedAt"]
            except Exception as err:
                log.error("error getting one video: %s, skipping...", err)

    def _get_uploads_playlist(self):
        return {
            "id": self.channel["contentDetails"]["relatedPlaylists"]["uploads"],
            "title": "uploads",
            "snippet": {"localized": {"title": "Uploads"}},
        }

    def forge_video_url(self, video_id):
        return "https://www.youtube.com/watch?v=" + video_id

    def get_channel_name(self):
        return self.channel["snippet"]["title"]

    @property
    def channel_data(self):
        return self._channel_data
