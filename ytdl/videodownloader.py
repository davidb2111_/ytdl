import sqlite3
from ytdl.youtubechannel import YoutubeChannel
from yt_dlp import YoutubeDL
import logging
from pathlib import Path

log = logging.getLogger(__name__)


class VideoDownloader:
    def __init__(
        self,
        channel: str,
        state_file,
        api_key: str,
        download: bool,
        update_db: bool,
        base_download_path: str = ".",
    ):
        self._youtube = YoutubeChannel(name=channel, api_key=api_key)
        log.info("Refreshing channel %s", self._youtube.get_channel_name())
        self._downloaded_video = []
        self._db = sqlite3.connect(state_file)
        self._download = download
        self._update_db = update_db
        self._base_download_path = base_download_path

    def _find_video_to_download(self):
        video_to_download = []
        for playlist_id, playlist in self._youtube.channel_data.items():
            for video_id, video in playlist["items"].items():
                if video_id not in self._downloaded_video:
                    video_to_download.append(
                        {
                            "id": video_id,
                            "url": self._youtube.forge_video_url(video_id),
                            "title": video["title"],
                            "playlist": playlist["title"],
                        }
                    )
        return video_to_download

    def start_download(self):
        for data in self._find_video_to_download():
            if not self._is_already_downloaded(data["id"]):
                print("Going to download %s" % data["url"])
                try:
                    # download here!
                    ydl_opts = {
                        "format": "mp4/mp3/best",
                        "paths": {"home": self._forge_download_path()},
                    }
                    if self._download:
                        with YoutubeDL(ydl_opts) as ydl:
                            ydl.download(data["url"])
                    if self._update_db:
                        self._mark_downloaded(data["id"], data["title"])
                except sqlite3.IntegrityError:
                    log.error("Already downloaded %s (%s)", data["title"], data["id"])
                except Exception as err:
                    log.error("Can't mark %s as downloaded: %s", data["id"], err)

    def _forge_download_path(self):
        return f"{self._base_download_path}/{self._youtube.get_channel_name()}/"

    def _mark_downloaded(self, video_id, title):
        self._db.execute("INSERT INTO downloaded VALUES (?, ?)", [video_id, title])
        self._db.commit()

    def _is_already_downloaded(self, video_id):
        res = self._db.execute("SELECT id FROM downloaded WHERE id = ?", [video_id])
        return len(res.fetchall())
